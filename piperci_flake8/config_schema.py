from marshmallow import fields, Schema, EXCLUDE


class Flake8ConfigSchema(Schema):

    flake8_conf = fields.String(required=False,
                                allow_none=True,
                                missing=None,
                                default=None)

    paths = fields.List(fields.String(required=True),
                        required=True)

    class Meta:
        unknown = EXCLUDE
