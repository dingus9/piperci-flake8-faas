import os
import subprocess

from flask import Request
from typing import Dict, Tuple, Union

from piperci.task.exceptions import PiperDelegateError, PiperError
from piperci.task.this_task import ThisTask


def executor(
    request: Request, task: ThisTask, config: dict
) -> Tuple[Union[Dict, str], int]:
    """
    Entrypoint to the flake8 task async executor function.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this task
    :param config:
    :return: (response_body, code)
    """
    task.info(str(task.task))
    # See piperci.task.this_task for gman.task interface usage docs
    try:
        task.info("flake8 task gateway handler.executor called successfully")

        # ## Replace below path with desired upload path
        execute_code(request, task, config)

        task.artifact(config["local_artifact_path"])
        task.complete("flake8 task complete")
        return {"test": "test"}, 200
    except PiperError:
        return "", 400


def execute_code(request: Request, task: ThisTask, config: dict):
    # Replace below with task execution logic

    def_or_cust = "custom" if config["flake8_conf"] else "default"
    config_file = "flake8.conf"

    if def_or_cust == "custom":

        task.artifact(config["flake8_conf"], upload_as="flake8_conf")

        with open(config_file, "w") as flake8_conf:
            flake8_conf.write(config["flake8_conf"])

    task.info(f"Executing flake8 with a {def_or_cust} config")

    for path in config["paths"]:
        if not os.path.exists(path):
            task.info("Path {path} does not exist")

    request_command = ["flake8", "--config", config_file, *config["paths"]]

    called_p = subprocess.run(request_command,
                              shell=True,
                              capture_output=True,
                              text=True)

    task.stderr(called_p.stderr)
    task.stdout(called_p.stdout)

    task.info(f"Command exited with return code {called_p.returncode}",
              return_code=called_p.returncode)


def gateway(request: Request, task: ThisTask, config: dict):
    """
    Entrypoint to the flake8 Task.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this task
    :return: (response_body, code)
    """

    try:
        task.info("flake8 task gateway handler.handle called successfully")
    except PiperError as e:
        return {"error": f"{str(e)}: no delegate was attempted"}, 400
    data = config["task_config"]
    try:
        task.delegate(config["executor_url"], data)
        return task.complete("flake8 gateway completed successfully")
    except PiperDelegateError as e:
        return {"error": f"{str(e)}"}, 400
